package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.And;
import cucumber.api.java.en.When;

public class CreateLeadPage extends Annotations{ 

	public CreateLeadPage() {
       PageFactory.initElements(driver, this);
	} 
	
	@FindBy(how=How.ID, using="createLeadForm_companyName") WebElement leadCompanyName; 
	@FindBy(how=How.ID, using="createLeadForm_firstName") WebElement leadFirstName;
	@FindBy(how=How.ID, using="createLeadForm_lastName") WebElement leadLastName;
	@FindBy(how=How.NAME, using="submitButton") WebElement clickCreateLead;
	
	//to get the company name
	@And("enter the company name as (.*)")
    public CreateLeadPage companyname(String data) {
    	clearAndType(leadCompanyName, data);
    	return this;    	
    }	
    
    //to get the first name 
    @And("enter the first name as (.*)")
    public CreateLeadPage firstName(String data) {
    	clearAndType(leadFirstName, data);
    	return this;
    }
    
    // to get the last name
    @And("enter the last name as (.*)")
    public CreateLeadPage lastName(String data) {
    	clearAndType(leadLastName, data);
    	return this;
    }
    
    //click create lead button
    @When("click the create lead")
    public ViewLeadPage createLeadClick() {
    	click(clickCreateLead);
    	return new ViewLeadPage();
    }
    
    //to modify the lead
    @FindBy(how=How.XPATH, using = "(//input[@class='smallSubmit'])[1]") WebElement updateClick;
	public CreateLeadPage updateLead() {
		click(updateClick);
		return this;
	}
	
	//to delete lead
	@FindBy(how=How.CLASS_NAME, using="subMenuButtonDangerous") WebElement deleteClick;
	public MyLeadPage deletelead() {
		click(deleteClick);
		return new MyLeadPage();
	}
}







