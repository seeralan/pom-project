package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.And;

public class HomePage extends Annotations{ 

	public HomePage() {
       PageFactory.initElements(driver, this);
	} 
	
	//crmsfa link click
	@FindBy(how=How.LINK_TEXT, using="CRM/SFA") WebElement crmsfaClick;
	@And("click crmsfa link")
    public MyHomePage clickcrmsfa() {
    	click(crmsfaClick);
    	return new MyHomePage();
    }
    
    //docs wiki link click
    @FindBy(how=How.LINK_TEXT, using="Docs Wiki") WebElement docswikiClick;
    public MainPage clickDocsWiki() {
    	click(docswikiClick);
    	return new MainPage();
    }
    
    //ecommerce link click
    @FindBy(how=How.LINK_TEXT, using="eCommerce") WebElement ecommClick;
    public EcommHomePage clickeCommerce() {
    	click(ecommClick);
    	return new EcommHomePage();
    }
}
    
   







