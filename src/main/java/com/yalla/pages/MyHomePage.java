package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.And;

public class MyHomePage extends Annotations{ 

	public MyHomePage() {
       PageFactory.initElements(driver, this);
	} 
		
	//click lead tab
	@FindBy(how=How.LINK_TEXT, using="Leads") WebElement myLeadClick;
	@And("click the lead tab")
    public MyLeadPage clickLead() {
    	click(myLeadClick);
    	return new MyLeadPage();
    }
}







