package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.And;

public class MyLeadPage extends Annotations{ 

	public MyLeadPage() {
       PageFactory.initElements(driver, this);
	} 
	
	//click create lead menu
	@FindBy(how=How.LINK_TEXT, using="Create Lead") WebElement createLeadClick;
	@And("click the create lead menu")
    public CreateLeadPage clickMylead() {
    	click(createLeadClick);
    	return new CreateLeadPage();
    }
    
  //to logout
    @FindBy(how=How.LINK_TEXT, using="Logout") WebElement eleLogout;
	public LoginPage clickLogout() {
		click(eleLogout);  
		return new LoginPage();
	}
}







