package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.Then;

public class ViewLeadPage extends Annotations{ 

	public ViewLeadPage() {
       PageFactory.initElements(driver, this);
	} 
	
	//to click the edit button in the view lead
	@FindBy(how=How.LINK_TEXT, using="Edit") WebElement editClick;
	public CreateLeadPage editlead() {
		click(editClick);
		return new CreateLeadPage();
	}	
	
	//to logout
	@FindBy(how=How.LINK_TEXT, using="Logout") WebElement eleLogout;
	public LoginPage clickLogout() {
		click(eleLogout);  
		return new LoginPage();
	}
	
	@Then("the create lead view screen should load")
	public void verifylead() {
		System.out.println("lead added");
	}
}







