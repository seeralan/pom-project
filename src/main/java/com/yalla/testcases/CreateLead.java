package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.LoginPage;
import com.yalla.testng.api.base.Annotations;

public class CreateLead extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC002_createlead";
		testcaseDec = "Create lead in leaftaps";
		author = "Seeralan";
		category = "smoke";
		excelFileName = "TC002";
	} 

	@Test(dataProvider="fetchData") 
	public void loginAndLogout(String uName, String pwd, String cName, String fname, String lname) {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd)
		.clickLogin()
		.clickcrmsfa()
		.clickLead()
		.clickMylead()
		.companyname(cName)
		.firstName(fname)
		.lastName(lname)
		.createLeadClick()
		.editlead()
		.updateLead()
		.deletelead()
		.clickLogout();
		
		/*LoginPage lp = new  LoginPage();
		lp.enterUserName();
		lp.enterPassWord();*/		
	}
	
}






